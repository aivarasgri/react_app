import React from 'react';
import './Posts.scss';
import post_img from '../../img/post_img.jpeg';
import ppt from '../../img/ppt.png';
import white from '../../img/white.png';
import city from '../../img/city.jpeg';
import street from '../../img/street.jpg';
import people from '../../img/people.jpeg';
import rr from '../../img/rr.jpg';

export const BlogPost1 = () => {
    return (
        <div className="Blog-Post1">
            <div className="Blog-header">
                <div className="Blog-header-img">
                    <img className="Blog-header-img" src={post_img} alt="Post-header-img" />
                </div>
                    <div className="Blog-header-par">
                        <p>Leadership Team</p><br/>
                        <p>80.20.15</p>
                    </div>
                    <div className="Post-flag">
                        <p>Blog</p>
                    </div>   
                </div>     
                <div className="Post-body">
                    <h2>Blog Tittle</h2>
                    <p>Lorizzle crackalackin i saw beyonces tizzles and my pizzle went crizzle sit amizzle, 
                        gizzle adipiscing yippiyo. Nullam sapizzle velizzle, phat sizzle, suscipizzle quis, 
                        yo mamma brizzle, arcu. 
                       </p>
                </div> 
                <div className="Document-container">
                    <img src={ppt} alt="Document-img" />
                        <div className="Blog-document-par">
                            <p>Presentation Name</p><br/>
                            <p>Filename.ppt</p>
                        </div>
                </div>
                <div className="Document-container">
                    <img src={ppt} alt="Document-img" />
                        <div className="Blog-document-par">
                            <p>Presentation Name</p><br/>
                            <p>Filename.ppt</p>
                        </div>
                </div>    
                <p className="Read-more-btn">Read more</p>  
        </div>
    );
}

export const BlogPost2 = () => {
    return (
        <div className="Blog-Post2">
            <div className="Blog-header">
                <div className="Blog-header-img">
                    <img className="Blog-header-img" src={post_img} alt="Post-header-img" />
                </div>
                    <div className="Blog-header-par">
                        <p>Leadership Team</p><br/>
                        <p>80.20.15</p>
                    </div>
                    <div className="Post-flag">
                        <p>News</p>
                    </div>   
                </div>     
                <div className="Post-body">
                    <h2>Nwes Tittle</h2>
                    <p>Lorizzle crackalackin i saw beyonces tizzles and my pizzle went crizzle sit amizzle, 
                        gizzle adipiscing yippiyo. Nullam sapizzle velizzle, phat sizzle, suscipizzle quis, 
                        yo mamma brizzle, arcu. 
                       </p> 
                </div> 
                <p className="Read-more-btn">Read more</p> 
        </div>
    );
}

export const BlogPost3 = () => {
    return (
        <div className="Blog-Post3">
            <div className="Blog-header">
                <div className="Blog-header-img">
                    <img className="Blog-header-img" src={post_img} alt="Post-header-img" />
                </div>
                    <div className="Blog-header-par">
                        <p>Leadership Team</p><br/>
                        <p>80.20.15</p>
                    </div>
                    <div className="Post-flag">
                        <p>News</p>
                    </div>   
                </div>     
                <div className="Post-body">
                    <h2>News Tittle</h2>
                    <p>Lorizzle crackalackin i saw beyonces tizzles and my pizzle went crizzle sit amizzle, 
                        gizzle adipiscing yippiyo. Nullam sapizzle velizzle, phat sizzle, suscipizzle quis, 
                        yo mamma brizzle, arcu. 
                       </p> 
                </div> 
                <p className="Read-more-btn">Read more</p> 
        </div>
    );
}

export const BlogPost4 = () => {
    return (
        <div className="Blog-Post4">
            <div className="Blog-header">
                <div className="Blog-header-img">
                    <img className="Blog-header-img" src={white} alt="Post-header-img" />
                </div>
                    <div className="Blog-header-par Blog-Post4-text-color">
                            <p>Retail Admin</p><br/>
                            <p>80.20.15</p>
                    </div>
                    <div className="Post-flag">
                        <p>News</p>
                    </div>   
                </div>     
                <div className="Post-body Blog-Post4-text-color">
                    <h2>Retail Details Issue 1</h2>
                </div> 
                <div className="Boby-img-container">
                     <img src={city} alt="blog-body"/>
                     <div className="Text-on-img"><span>By</span></div>
                </div>    
                <p className="Read-more-btn">Read more</p>  
        </div>
    );
}

export const BlogPost5 = () => {
    return (
        <div className="Blog-Post5">
            <div className="Blog-header">
                <div className="Blog-header-img">
                    <img className="Blog-header-img" src={post_img} alt="Post-header-img" />
                </div>
                    <div className="Blog-header-par">
                        <p>Leadership Team</p><br/>
                        <p>80.20.15</p>
                    </div>
                    <div className="Post-flag">
                        <p>News</p>
                    </div>   
                </div>     
                <div className="Post-body">
                    <h2>Blog Tittle</h2>
                    <p>Lorizzle crackalackin i saw beyonces tizzles and my pizzle went crizzle sit amizzle, 
                        gizzle adipiscing yippiyo.
                       </p>
                </div> 
                <div className="Boby-img-container-post5">
                     <img src={street} alt="blog-body"/>
                </div>    
                <p className="Read-more-btn">Read more</p>  
        </div>
    );
}

export const BlogPost6 = () => {
    return (
        <div className="Blog-Post6">
            <div className="Blog-header">
                <div className="Blog-header-img">
                    <img className="Blog-header-img" src={rr} alt="Post-header-img" />
                </div>
                    <div className="Blog-header-par">
                        <p>Rondall Roberts</p><br/>
                        <p>Account exec</p><br/><br/>
                        <p>80.20.15</p>
                    </div>
                    <div className="Post-flag Post-flag-color">
                        <p>Chirp</p>
                    </div>   
                </div>     
                <div className="Post-body Post6-boby-par">
                    <p><span>''</span>Lorizzle crackalackin i saw beyonces tizzles and my pizzle went crizzle sit amizzle, 
                        gizzle adipiscing yippiyo. Nullam sapizzle velizzle.
                       </p> 
                </div> 
        </div>
    );
}

export const BlogPost7 = () => {
    return (
        <div className="Blog-Post7">
            <div className="Blog-header">
                <div className="Blog-header-img">
                    <img className="Blog-header-img" src={post_img} alt="Post-header-img" />
                </div>
                    <div className="Blog-header-par">
                        <p>Leadership Team</p><br/>
                        <p>80.20.15</p>
                    </div>
                    <div className="Post-flag">
                        <p>News</p>
                    </div>   
                </div>     
                <div className="Post-body">
                    <h2>Blog Tittle</h2>
                    <p>Lorizzle crackalackin i saw beyonces tizzles and my pizzle went crizzle sit amizzle, 
                        gizzle adipiscing yippiyo.
                       </p>
                </div> 
                <div className="Boby-img-container">
                     <img src={people} alt="blog-body"/>
                     <div className="Text-on-img"><span>By</span></div>
                </div>    
                <p className="Read-more-btn">Read more</p>  
        </div>
    );
}