import React from 'react';
import './NavigationItems.scss';

export const NavItems = () => {
    const list = ['Quick Launch', 'Directory', 'Blog', 'Departments', 'Library'];
        const Items = list.map((item, i) => {
            return (
                <li className="Nav-items" key={`${item} ${i}`}> 
                    {item}
                </li>
             )
         });

        return (
            <div className="Nav__Item-Container">
                <ul>
                    {Items}
                    <input type="text" placeholder="Search" name="search" />
                </ul>
            </div>
        );
    }