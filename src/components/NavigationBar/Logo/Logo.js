import React from 'react';
import './Logo.scss';
import logo from '../../../img/logo1.png';

export const NavLogo = () => {
    return (
        <div className="logo__wrapper">
            <img className="logo" src={logo} alt="logo"/>
            <p>Banana Bank</p>
        </div>
    );
}