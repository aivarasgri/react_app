import React from 'react';
import './NavigationBar.scss';
import {NavLogo} from './Logo/Logo.js';
import {NavItems} from './NavigationItems/NavigationItems.js';

const NavigationBar = () => {
    return (
        <div className="NavBar">
            <div className="NavBar-container">
                <div className="Logo-container">
                    <NavLogo />
                    <NavItems />
               </div>
            </div>       
        </div>
    );
}

export default NavigationBar;