import React from 'react';
import './Options.scss';
import SimpleList from '../OptionsList/OptionsList.js';

const options = () => {
    return (
        <div className="Options">
            <SimpleList />
        </div>
    ); 
}



export default options;