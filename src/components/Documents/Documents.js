import React from 'react';
import './Documents.scss';
import ppt from '../../img/ppt.png';
import xls from '../../img/xls.png';
import pdf from '../../img/pdf.png';
import word from '../../img/word.png';

export const DocTittle = () => {
    return (   
        <div className="Options Document-title">
            <p>Popular Documents</p>
        </div>
    );
}

export const DocList = () => {
    var docsArray = [
        { name: 'Benefits new form (pptx)', image: ppt }, 
        { name: 'Document form for new workers', image: xls }, 
        { name: 'Benefits new form (pptx)', image: pdf }, 
        { name: 'Document form PDF form', image: word },
        { name: 'Benefits new form (pptx)', image: ppt }, 
        { name: 'Document form for new workers', image: xls }, 
        { name: 'Benefits new form (pptx)', image: pdf }, 
        { name: 'Document form PDF form', image: word },
        { name: 'Benefits new form (pptx)', image: ppt }, 
        { name: 'Document form for new workers', image: xls }, 
        { name: 'Benefits new form (pptx)', image: pdf }, 
        { name: 'Document form PDF form', image: word }
      ];
    
        const list = docsArray.map((list, id) => {
            return (
                    <li className="List-container" key={`${list.name} ${id}`}>
                        <img src={list.image} alt='documents' width={30}/> 
                        <div className="list-paragraph">{list.name}</div>
                    </li>
                )
            });

            return ( 
                <div className="Document-list">
                    <ul>
                        {list}
                    </ul>
                </div>
            )

        }

            

