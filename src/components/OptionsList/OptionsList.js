import React from 'react';
import './OptionsList.scss';

export const SimpleList = () => {
  const list = ['All', 'News', 'Blog', 'Chips', 'Docs', 'Retail Details'];
  const items = list.map((item, i) => {
      return (
        <li className="Navigation-items" key={`${item} ${i}`}>
          {item}
        </li>
      )});

  return (
    <div>
      <ul className="Navigation__item-container">
        {items}
      </ul>
    </div>
  )
};

export default SimpleList;