import React from 'react';
import './App.scss';
import './components/Options/Options.scss';
import Options from './components/Options/Options.js';
import {DocTittle, DocList} from './components/Documents/Documents.js';
import NavigationBar from './components/NavigationBar/NavigationBar';
import {BlogPost1, BlogPost2, BlogPost3, BlogPost4, BlogPost5, BlogPost6, BlogPost7} from './components/Posts/Posts';

function App() {
  return (
    <div className="App">
       <div className="wrapper"> 
          <div className="navbar">
              <NavigationBar />
          </div>
          <div className="options">
              <Options />
          </div> 
          <div className="doctittle">       
              <DocTittle />
          </div>
          <div className="docList">       
              <DocList />
          </div>
            <div className="First-blog-post">
              <BlogPost1 />
            </div> 
            <div className="Second-blog-post">      
                 <BlogPost2 />
            </div>      
            <div className="Third-blog-post">      
                 <BlogPost3 />
            </div>
            <div className="Forth-blog-post">      
                 <BlogPost4 />
            </div>
            <div className="Fifth-blog-post">      
                 <BlogPost5 />
            </div>
            <div className="Sixth-blog-post">      
                 <BlogPost6 />
            </div>
            <div className="Seventh-blog-post">      
                 <BlogPost7 />
            </div>                
       </div> 
    </div>
  );
}

export default App;
